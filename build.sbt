import sbt.Keys.{licenses, publishMavenStyle, version}
import sbtrelease.ReleaseStateTransformations._

scalaVersion := "2.13.1"

organization := "com.impactua"
name := "play2-auth"

version := (version in ThisBuild).value
licenses += ("MIT", url("http://opensource.org/licenses/MIT"))
publishMavenStyle := true
publishArtifact := true
publishArtifact in Test := false
bintrayReleaseOnPublish := true
bintrayPackage := name.value
bintrayOrganization in bintray := Some("sergkh")
releaseIgnoreUntrackedFiles := true
crossScalaVersions := Seq("2.12.10", "2.13.1")

resolvers += "scalaz-bintray"       at "http://dl.bintray.com/scalaz/releases"
resolvers += "Typesafe repository"  at "http://repo.typesafe.com/typesafe/releases/"
resolvers += "Sonatype snapshots"   at "https://oss.sonatype.org/content/repositories/snapshots/"

val playVersion = "2.8.0"

libraryDependencies ++= Seq(
  "com.typesafe.play"         %% "play-guice"             % playVersion % Provided,
  "com.typesafe.play"         %% "play"                   % playVersion % Provided,
  "com.pauldijou"             %% "jwt-core"               % "4.2.0",
  "ch.qos.logback"             % "logback-classic"        % "1.2.3"     % Test,
  "org.scalatestplus.play"    %%  "scalatestplus-play"    % "5.0.0"     % Test
)

releaseProcess := Seq[ReleaseStep](
  checkSnapshotDependencies,
  inquireVersions,
  runClean,
  runTest,
  setReleaseVersion,
  commitReleaseVersion,
  tagRelease,
  publishArtifacts,
  setNextVersion,
  commitNextVersion,
  pushChanges
)