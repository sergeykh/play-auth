import com.impactua.bouncer.commons.security.HmacConfirmationProvider
import org.scalatest.{MustMatchers, OptionValues, WordSpec}
import play.api.Configuration
import play.api.test.FakeRequest

class ConfirmationVerifierSpec extends WordSpec with MustMatchers {

  "Confirmation Verifier" should {
    "encode and decode header" in {
      val p = new HmacConfirmationProvider(Configuration.from(Map("play.http.secret.key" -> "secret-password-1")))

      val header = p.confirmationHeader("123456", "secret-password-1")

      p.verifyConfirmed(FakeRequest().withHeaders(header)) mustBe true
    }

    "do not decode headers with invalid signature" in {
      val p = new HmacConfirmationProvider(Configuration.from(Map("play.http.secret.key" -> "secret-password-1")))

      val header = p.confirmationHeader("123456", "secret-password-2")

      p.verifyConfirmed(FakeRequest().withHeaders(header)) mustBe false
    }
  }

}
