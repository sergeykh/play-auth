import java.util.Base64

import com.impactua.bouncer.commons.models.User
import com.impactua.bouncer.commons.models.exceptions.AppException
import com.impactua.bouncer.commons.security.{JWTUserEncoder, JWTUserProvider}
import com.typesafe.config.ConfigFactory
import org.scalatest.{MustMatchers, WordSpec}
import pdi.jwt.{Jwt, JwtAlgorithm}
import play.api.Configuration
import play.api.libs.json.Json
import play.api.test.FakeRequest

import scala.util.Random

class UserEncoderSpec extends WordSpec with MustMatchers {

  "JWT User Encoder" should {
    "encode user that can be decoded by UserProvider" in {

      val encoder = new JWTUserEncoder("qwerty")
      val provider = new JWTUserProvider(Configuration(ConfigFactory.parseString("""play.http.secret.key = qwerty""")))

      val original = User(Random.nextLong())

      val request = FakeRequest().withHeaders("x-auth" -> encoder.encode(original))
      val user = provider.fromRequest(request)

      user mustBe original
    }
  }

  "JWT User Provider" should {
    "support unsecure mode" in {
      val provider = new JWTUserProvider(Configuration(ConfigFactory.parseString(
        """
          | play.http.secret.key = qwerty
          | security.mode = unsecure
        """.stripMargin)))

      val original = User(Random.nextLong())

      val base64 = Base64.getEncoder

      val request = FakeRequest().withHeaders("x-auth" ->
        base64.encodeToString(Json.stringify(Json.toJson(original)).getBytes)
      )

      val user = provider.fromRequest(request)
      user mustEqual original
    }

    "support unsecure mode with pre-encoded user" in {
      val provider = new JWTUserProvider(Configuration(ConfigFactory.parseString(
        """
          | play.http.secret.key = qwerty
          | security.mode = unsecure
        """.stripMargin)))

      val request = FakeRequest().withHeaders("x-auth" -> "eyJ1dWlkIjogNCwgImVtYWlsIjogInRlc3RAZ21haWwuY29tIn0=")
      val user = provider.fromRequest(request)
      user mustEqual User(4, email = Some("test@gmail.com"))
    }

    "decode token" in {
      val token = """eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1dWlkIjoyOTA0NDE4NDY5NDk3NTY0LCJlbWFpbCI6InNlcmdleS5raHJ1c2NoYWtAZ21haWwuY29tIiwicGhvbmUiOiIrMzgwOTc5NDczMzUwIiwiZmxhZ3MiOlsiQURNSU4iLCJMT0dJTl9DT05GSVJNRUQiXSwicm9sZXMiOlsiQURNSU4iXSwicGVybWlzc2lvbnMiOlsib2F1dGhfdG9rZW46Y3JlYXRlIiwiaW50ZXJuYWxfd2ViIiwidXNlcnM6cmVhZCIsInVzZXJzOmVkaXQiLCJyZWFkOnVzZXJzIiwiZWRpdDp1c2VycyIsInJlYWQ6Z2F0ZXdheXMiLCJlZGl0OmdhdGV3YXlzIiwicmVhZDppbnZvaWNlcyIsImVkaXQ6aW52b2ljZXMiLCJyZWFkOm5ld3MiLCJlZGl0Om5ld3MiLCJyZWFkOnR4IiwiZWRpdDp0eCIsInJlYWQ6cmF0ZXMiLCJlZGl0OnJhdGVzIiwicmVhZDpzZXJ2aWNlcyIsImVkaXQ6c2VydmljZXMiLCJyZWFkOnRlbXBsYXRlcyIsImVkaXQ6dGVtcGxhdGVzIiwicmVhZDp3b3JrZmxvdyIsImVkaXQ6d29ya2Zsb3ciLCJiaXRjb2luczpyZWFkIiwiYml0Y29pbnM6d3JpdGUiLCJzbXM6bm90X3JlcXVpcmVkIiwicmVwb3J0czplZGl0IiwicmVwb3J0czpyZWFkIiwicmVwb3J0czpleGVjdXRlIiwic3RvcmU6d3JpdGUiLCJzdG9yZTphZG1pbiIsImFkbWluOnRlbXBsYXRlcyIsInJlYWQ6YWNjb3VudF9saW1pdHMiLCJlZGl0OmFjY291bnRfbGltaXRzIiwiYXBwbGljYXRpb246YWRtaW4iLCJlZGl0OmVtaXNzaW9ucyIsInJlYWQ6ZW1pc3Npb25zIiwicmVhZDpwYXl0aWNrZXRzIiwiZWRpdDpwYXl0aWNrZXRzIiwiZGF0YXNvdXJjZXM6ZWRpdCIsImRhdGFzb3VyY2VzOnJlYWQiLCJwZXJtaXNzaW9uczplZGl0IiwicGVybWlzc2lvbnM6cmVhZCIsImRpY3Rpb25hcmllczplZGl0IiwiZGljdGlvbmFyaWVzOnJlYWQiLCJzd2FnZ2VyOmVkaXQiLCJzd2FnZ2VyOnJlYWQiXSwiY3JlYXRlZCI6MTQ1MTA0NDkyMTE3Nn0.LWo4ifKRwDG95PvpF2C-V-lt6T5u22DJFN3nv7GY0_M"""

      val provider = new JWTUserProvider(Configuration(ConfigFactory.parseString("""play.http.secret.key="cv12S0p7f_bo=31bcgmkhdfgskkQfdrk;iT3Bytm4@YEMQhvalUZIK0706Wkvvvas"""")))

      val request = FakeRequest().withHeaders("x-auth" -> token)
      val user = provider.fromRequest(request)

      user.uuid mustEqual 2904418469497564L
    }

    "fail on expired token" in {
      val token = Jwt.encode("""{"exp": 1530577625, "uuid": 1204418469497564, "email": "test1@mail.com"}""", "cv12S0p7f_bo=31bcgmkhdfgskkQfdrk;iT3Bytm4@YEMQhvalUZIK0706Wkvvvas", JwtAlgorithm.HS256)

      val provider = new JWTUserProvider(Configuration(ConfigFactory.parseString("""play.http.secret.key="cv12S0p7f_bo=31bcgmkhdfgskkQfdrk;iT3Bytm4@YEMQhvalUZIK0706Wkvvvas"""")))

      val request = FakeRequest().withHeaders("x-auth" -> token)

      assertThrows[AppException[_]] {
        provider.fromRequest(request)
      }
    }
  }
}
