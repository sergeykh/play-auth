import com.impactua.bouncer.commons.utils.FormConstraints
import org.scalatest.{Matchers, MustMatchers, WordSpec}
import play.api.data.validation.Valid

class FormConstraintsSpec extends WordSpec with Matchers {
  import FormConstraints._

  "A phone validator" should {

    "accept valid phones" in {
      phoneNumber("+380943453412") shouldEqual Valid
      phoneNumber("+15109450777") shouldEqual Valid
      optPhoneNumber(Some("+380943453412")) shouldEqual Valid
      optPhoneNumber(Some("+15109450777")) shouldEqual Valid
    }

    "not accept invalid phones" in {
      phoneNumber("+15109450777asfsdg") should not be Valid
      phoneNumber("3554+15109450777") should not be Valid
      optPhoneNumber(Some("+15109450777asfsdg")) should not be Valid
      optPhoneNumber(Some("3554+15109450777asfsdg")) should not be Valid
    }


  }

}
