import com.impactua.bouncer.commons.models.User
import org.scalatest.{Matchers, WordSpec}
import play.api.libs.json.Json

class UserSpec extends WordSpec with Matchers {

  import com.impactua.bouncer.commons.models.User._

  "User" should {

    "be deserializable from json" in {
      val user = User(
        uuid = 2134443645645546L,
        email = Some("email@user.com"),
        phone = Some("+440989452367"),
        firstName = Some("John"),
        lastName = Some("Jonson"),
        flags = Seq("ANY_STRING", "ADMIN", "BLOCKED"),
        hierarchy = Seq("test", "test2"),
        permissions = Seq("users:edit", "world:conquer", "settings:break")
      )

      val userJson = Json.parse(
        """{ "uuid": 2134443645645546,
          |"email" : "email@user.com",
          |"phone": "+440989452367",
          |"firstName": "John",
          |"lastName": "Jonson",
          |"flags" : [ "ANY_STRING", "ADMIN", "BLOCKED" ],
          |"hierarchy" : [ "test", "test2" ],
          |"permissions": ["users:edit", "world:conquer", "settings:break"]
          |}""".stripMargin)

      userJson.as[User] shouldEqual user
      Json.toJson(user) shouldEqual userJson
    }

    "be serializable with minimum set of fields" in {
      val user = User(uuid = 1000000000000001L)

      val userJson = Json.parse("""{ "uuid": 1000000000000001}""".stripMargin)

      userJson.as[User] shouldEqual user
      Json.toJson(user) shouldEqual userJson
    }

    "support short form" in {

      val userJson = Json.parse(
        """{ "id": 2134443645645546,
          |"em" : "email@user.com",
          |"ph": "+440989452367",
          |"fn": "John",
          |"ln": "Jonson",
          |"rol": [ "ADMIN", "TEST"],
          |"flg" : [ "ANY_STRING", "ADMIN", "BLOCKED" ],
          |"hrc" : [ "test", "test2" ],
          |"prm": ["users:edit", "world:conquer", "settings:break"]
          |}""".stripMargin)

      userJson.as[User] shouldEqual User(
        2134443645645546L,
        Some("email@user.com"),
        Some("+440989452367"),
        Some("John"),
        Some("Jonson"),
        Seq("ADMIN", "TEST"),
        Seq("ANY_STRING", "ADMIN", "BLOCKED"),
        Seq("test", "test2"),
        Seq("users:edit", "world:conquer", "settings:break")
      )
    }
  }
}
