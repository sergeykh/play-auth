import com.impactua.bouncer.commons.utils.RandomStringGenerator
import org.scalatest.{MustMatchers, WordSpec}

class RandomStringGeneratorSpec extends WordSpec with MustMatchers {
  import RandomStringGenerator._

  "A random generator" should {

    "generate numeric passwords" in {
      generateNumericPassword(5,5).length mustEqual 5       
    }

    "generate URL safe IDs" in {
      val uid = generateId()
      uid.length mustEqual 21
    }
  }

}
