package com.impactua.bouncer.commons.security

import com.impactua.bouncer.commons.models.User
import pdi.jwt.{Jwt, JwtAlgorithm}
import play.api.libs.json.Json

class JWTUserEncoder(secret: String) {

  def encode(user: User): String =
    Jwt.encode(Json.stringify(Json.toJson(user)), secret, JwtAlgorithm.HS256)

}
