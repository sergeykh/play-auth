package com.impactua.bouncer.commons.security

import java.util.Base64

import javax.inject.{Inject, Singleton}
import com.google.inject.ImplementedBy
import com.impactua.bouncer.commons.models.exceptions.AppException
import com.impactua.bouncer.commons.models.{ResponseCode, User}
import com.impactua.bouncer.commons.utils.Logging
import pdi.jwt.JwtAlgorithm.{HS384, HS512}
import pdi.jwt.{Jwt, JwtAlgorithm}
import play.api.Configuration
import play.api.libs.json.Json
import play.api.mvc.RequestHeader

import scala.util.{Failure, Success, Try}


@ImplementedBy(classOf[JWTUserProvider])
trait UserProvider {
  def fromRequest(req: RequestHeader): User
}

@Singleton
class JWTUserProvider @Inject()(config: Configuration) extends UserProvider with Logging {

  val secret = config.getOptional[String]("play.http.secret.key").getOrElse(throw new RuntimeException("Undefined play.http.secret.key value"))

  val securedMode = config.getOptional[String]("security.mode").getOrElse("secure") != "unsecure"

  if (!securedMode) {
    log.warn("!!! WARNING !!!")
    log.warn("Unsecure mode enabled by config option security.mode = unsecure")
  }

  val algorithms = Seq(JwtAlgorithm.HS256, HS384, HS512)

  def fromRequest(request: RequestHeader) = {
    request.headers.get("x-auth") map { header =>

      if (securedMode) {
        Jwt.decodeRawAll(header, secret, algorithms) match {
          case Success((_, body, _)) =>
            try {
              Json.parse(body).as[User]
            } catch {
              case ex: Exception =>
                log.warn("Wrong JWT token format: " + header, ex)
                throw AppException(ResponseCode.AUTHORIZATION_FAILED, "Wrong JWT token format: " + header)
            }
          case Failure(ex) =>
            log.warn("Fraudulent JWT token: " + header, ex)
            throw AppException(ResponseCode.AUTHORIZATION_FAILED, "Fraudulent JWT token: " + header)
        }
      } else {
        val base64 = Base64.getDecoder
        val json = new String(base64.decode(header))
        Json.parse(json).as[User]
      }
    } getOrElse {
      throw AppException(ResponseCode.AUTHORIZATION_FAILED, "Authorization header not specified")
    }
  }
}
