package com.impactua.bouncer.commons.security

import javax.inject.Inject

import com.impactua.bouncer.commons.models.exceptions.AppException
import com.impactua.bouncer.commons.models.{ResponseCode, User}
import play.api.Logger
import play.api.mvc.Security.AuthenticatedBuilder
import play.api.mvc._

import scala.concurrent.{ExecutionContext, Future}
import scala.util.Try

/**
  *
  * @author Yaroslav Derman <yaroslav.derman@gmail.com>
  *         created on 29/04/16
  */
class Authorization @Inject()(userProvider: UserProvider,
                              bodyParser: BodyParsers.Default
                             )(implicit ctx: ExecutionContext) {

  private val log = Logger(this.getClass.getName)

  case class UserOptRequest[A](optUser: Option[User], request: Request[A]) extends WrappedRequest[A](request)

  object OptSecured extends ActionBuilder[UserOptRequest, AnyContent] with ActionTransformer[Request, UserOptRequest] {

    def transform[A](request: Request[A]) = {
      Future.successful {
        UserOptRequest(Try(userProvider.fromRequest(request)).toOption, request)
      }
    }

    override def parser: BodyParser[AnyContent] = bodyParser

    override protected def executionContext: ExecutionContext = ctx
  }

  object Secured extends AuthenticatedBuilder(
    userRequest => Option(userProvider.fromRequest(userRequest)),
    defaultParser = bodyParser,
    onUnauthorized => onUnauthorizedRequest(onUnauthorized)
  )

  case class SecuredOwner(userAnyId: String, meSupport: Boolean = true) extends AuthenticatedBuilder(
    userRequest => {
      val user = userProvider.fromRequest(userRequest)
      if ((meSupport && "me" == userAnyId) || user.checkIdentifier(userAnyId)) Some(user) else None
    },
    defaultParser = bodyParser,
    onUnauthorized => onUnauthorizedRequest(onUnauthorized)
  )

  /**
    * Authorized user by it's ID or Administrator if it has any of specified permissions
    */
  case class SecuredOwnerOrAnyPermission(userAnyId: String, meSupport: Boolean = true, permissions: List[String] = Nil) extends AuthenticatedBuilder(
    userRequest => {
      val user = userProvider.fromRequest(userRequest)
      if ((meSupport && "me" == userAnyId) || user.checkIdentifier(userAnyId) || permissions.exists(user.hasPermission)) Some(user) else None
    },
    defaultParser = bodyParser,
    onUnauthorized => onUnauthorizedRequest(onUnauthorized)
  )

  class SecuredPermissions(checkAll: Boolean, p: String*) extends AuthenticatedBuilder(
    userRequest => {
      val user = userProvider.fromRequest(userRequest)
      val permissions = p.map(_.toUpperCase)

      if (checkAll && permissions.forall(user.hasPermission)) {
        Some(user)
      } else if (!checkAll && permissions.exists(user.hasPermission)) {
        Some(user)
      } else {
        None
      }
    },
    defaultParser = bodyParser,
    onUnauthorized => onUnauthorizedRequest(onUnauthorized)
  )

  case class SecuredAllPermissions(permissions: String*) extends SecuredPermissions(true, permissions: _*)

  case class SecuredAnyPermissions(permissions: String*) extends SecuredPermissions(false, permissions: _*)

  case class SecuredComposition(block: User => Boolean) extends AuthenticatedBuilder(
    userRequest => {
      val user = userProvider.fromRequest(userRequest)
      Some(user).filter(block)
    },
    defaultParser = bodyParser,
    onUnauthorized => onUnauthorizedRequest(onUnauthorized)
  )

  private def onUnauthorizedRequest(req: RequestHeader) = {
    log.warn(s"Unauthorized request for ${req.method} ${req.uri}")
    throw AppException(ResponseCode.ACCESS_DENIED, "Unauthorized request")
  }

}