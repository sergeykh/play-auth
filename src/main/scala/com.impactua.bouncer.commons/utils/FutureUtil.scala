package com.impactua.bouncer.commons.utils

import com.impactua.bouncer.commons.models.ResponseCode
import com.impactua.bouncer.commons.models.exceptions.AppException
import com.impactua.bouncer.commons.models.exceptions.AppException.CodeConverter

import scala.annotation.implicitNotFound
import scala.concurrent.{ExecutionContext, Future}

object FutureUtil {
  @implicitNotFound("Provide an implicit instance of converter from custom error code into HTTP code")
  def appFail[T, E](code: E, message: String)(implicit cc: CodeConverter[E]): Future[T] = Future.failed(AppException[E](code, message))

  def failNotFound[T](message: String): Future[T] = Future.failed(AppException(ResponseCode.ENTITY_NOT_FOUND, message))
  def failAccessDenied[T](message: String): Future[T] = Future.failed(AppException(ResponseCode.ACCESS_DENIED, message))

  def futureV[T](o: T): Future[T] = Future.successful(o)
  def futureSome[T](o: T): Future[Option[T]] = Future.successful(Some(o))

  def conditional[A](cond: Boolean, f: => Future[A]): Future[_] = if (cond) f else Future.unit

  def conditionalFail[A, E](cond: Boolean, code: E, message: String)(implicit cc: CodeConverter[E]): Future[_] = if (cond) appFail(code, message) else Future.unit

  implicit class RichFuture[A](val f: Future[A]) extends AnyVal {
    /** Future sequence operator (monad sequence).
      * Executes both, returns result of the second Future */
    def >>[B](f2: => Future[B])(implicit ec: ExecutionContext): Future[B] = f.flatMap(_ => f2)

    /** Future sequence operator (monad sequence).
      * Executes both, returns result of the first Future */
    def <<[B](f2: => Future[B])(implicit ec: ExecutionContext): Future[A] = for { a <- f; _ <- f2 } yield a
  }
}
