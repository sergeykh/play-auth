package com.impactua.bouncer.commons.utils

import com.impactua.bouncer.commons.models.exceptions.FormValidationException
import play.api.data.Form
import play.api.mvc._

import scala.language.implicitConversions

/**
  * Util object containing predefined common server responses and response builders.
  * @author Sergey Khruschak
  * @author Yaroslav Derman
  */
object RichRequest {

  private val HTTP_IP_HEADERS = Seq(
    "X-Forwarded-For", "X-Real-IP", "Proxy-Client-IP", "HTTP_CLIENT_IP", "HTTP_X_FORWARDED_FOR", "WL-Proxy-Client-IP"
  )

  implicit class RichRequest(val r: RequestHeader) extends AnyVal {
    def asForm[T](form: Form[T]): T = r match {
      case req: Request[_] => form.bindFromRequest()(req).fold(
        error => throw FormValidationException(error),
        data => data
      )
      case _ => throw new RuntimeException("Unsupported request header type for form extracting")
    }

    /**
      * Binds a form only from a query parameters, without checking a body.
      * Required if GET/DELETE has invalid Content-Type
      *
      * @param form form to bind
      * @tparam T form type
      * @return binded data or exception
      */
    def asQueryForm[T](form: Form[T]): T = r match {
      case req: Request[_] => form.bindFromRequest(req.queryString).fold(
        error => throw FormValidationException(error),
        data => data
      )
      case _ => throw new RuntimeException("Unsupported request header type for form extracting")
    }

    def clientIp: String = {
      val header = HTTP_IP_HEADERS.find(name => r.headers.get(name).exists(h => h.nonEmpty && !h.equalsIgnoreCase("unknown")))

      header match {
        case Some(name) =>
          val header = r.headers(name)
          if (header.contains(",")) header.split(",").head else header
        case None       => r.remoteAddress
      }
    }

    def clientAgent: String = r.headers.get("User-Agent").getOrElse("not set")
  }

}
