package com.impactua.bouncer.commons.utils

import java.util.Calendar
import java.util.regex.Pattern

import play.api.data.Forms._
import play.api.data.validation._

import scala.util.matching.Regex

/**
  *
  * @author Yaroslav Derman <yaroslav.derman@gmail.com>
  *         created on 26/07/16
  */
object FormConstraints extends Constraints {
    val EMAIL_VALIDATION_PATTERN = Pattern.compile("""^[a-zA-Z0-9\.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$""")
    val PHONE_VALIDATION_PTRN = """^(\+\d{10,15})$""".r

    private final val MIN_PASSWORD_LENGTH = 6
    private final val MAX_NUMBER_LIMIT = 100

    private final val ERROR_PHONE_EMPTY = "error.phone.empty"
    private final val ERROR_PASSWORD_FORMAT = "error.password.format"

    val longUuid = longNumber.verifying(pattern("\\d{16}"))
    val limit = number(min = 1, max = MAX_NUMBER_LIMIT)
    val offset = number(min = 0)

    val emptyStr: Constraint[Option[String]] = Constraint("constraint.empty") { field: Option[String] =>
      field.filter(_.isEmpty).map(_ => Valid).getOrElse(Invalid(ValidationError("error.expected.empty")))
    }

    def and[T](constraints: Constraint[T]*): Constraint[T] = Constraint("constraint.and") { field: T =>
      constraints.map(_.apply(field)).dropWhile(_ == Valid) match {
        case Nil                          => Valid
        case list: Seq[ValidationResult]  => list.head
      }
    }

    def or[T](constraints: Constraint[T]*): Constraint[T] = Constraint("constraint.or") { field: T =>
      val validationResults = constraints.map(_.apply(field))
      validationResults.find(_ == Valid) match {
        case Some(valid) => Valid
        case None => Invalid(validationResults.filterNot(_ == Valid).map(_.asInstanceOf[Invalid]).flatMap(_.errors))
      }
    }

    def anyOf[T](args: T*): Constraint[T] = Constraint("constraints.anyOf"){ field: T =>
      if (args.contains(field)){
        Valid
      } else {
        Invalid(ValidationError("error.expected.option", args))
      }
    }

    def unique[T](f: T => Boolean, name: String): Constraint[T] = Constraint("constraint.unique." + name) { field: T =>
      if (f(field)) {
        Valid
      } else {
        Invalid(ValidationError("error.unique." + name))
      }
    }

    def phoneNumber: Constraint[String] = Constraint[String]("constraint.phone") { e =>
      Option(e) match {
        case Some(phone) => validateField(phone, PHONE_VALIDATION_PTRN, ERROR_PHONE_EMPTY, "error.phone.invalid")
        case None        => Invalid(ValidationError(ERROR_PHONE_EMPTY))
      }
    }

    def optPhoneNumber: Constraint[Option[String]] = Constraint[Option[String]]("constraint.phone") {
      case Some(phone) => validateField(phone, PHONE_VALIDATION_PTRN, ERROR_PHONE_EMPTY, "error.phone.invalid")
      case None        => Valid
    }

    private val emailRegex = """^[a-zA-Z0-9\.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$""".r
    def optEmailAddress: Constraint[Option[String]] = Constraint[Option[String]]("constraint.email") {
      case Some(e) => validateField(e, emailRegex, "error.email", "error.email")
      case None    => Valid
    }

    private val nonEmptyRegex = """.+""".r
    def nonEmptyStr: Constraint[Option[String]] = Constraint[Option[String]]("constraint.nonempty") {
      case Some(str) => validateField(str, nonEmptyRegex, "error.string.empty", "error.expected.nonempty")
      case None        => Valid
    }

    private def validateField(field: String, regex: Regex, emptyMsg: String, invalidMsg: String) = {
      field.trim match {
        case empty if empty.isEmpty => Invalid(ValidationError(emptyMsg))
        case regex(_*) => Valid
        case _ => Invalid(ValidationError(invalidMsg))
      }
    }

    val password = nonEmptyText(minLength = MIN_PASSWORD_LENGTH).verifying(passwordConstraint)

    private val allNumbers = """\d*""".r
    private val allLetters = """[A-Za-z]*""".r
    private def passwordConstraint: Constraint[String] = Constraint("constraints.password") {
      case allNumbers() => Invalid(Seq(ValidationError(ERROR_PASSWORD_FORMAT)))
      case allLetters() => Invalid(Seq(ValidationError(ERROR_PASSWORD_FORMAT)))
      case _ => Valid
    }

    val cardYear: Constraint[Int] = Constraint("constraints.card.year"){ year =>
      val current = Calendar.getInstance()
      if (current.get(Calendar.YEAR) % 100 > year) {
        Invalid(ValidationError("error.creditCard.expire.year"))
      } else {
        Valid
      }
    }

    def pattern[T](regex: String): Constraint[T] = Constraint[T] { e: T =>
      if (e.toString.matches(regex)){
        Valid
      } else {
        Invalid(ValidationError("error.regexp", e))
      }
    }

  }
