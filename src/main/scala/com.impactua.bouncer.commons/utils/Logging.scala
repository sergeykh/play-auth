package com.impactua.bouncer.commons.utils

import play.api.Logger

/**
  * Logging mixin.
  *
  * @author Yaroslav Derman <yaroslav.derman@gmail.com>
  *         created on 26/07/16
  */
trait Logging {

  val log = Logger(this.getClass.getName)

}
