package com.impactua.bouncer.commons.utils

import com.typesafe.config.{Config, ConfigObject, ConfigValue, ConfigValueFactory, ConfigValueType}
import play.api.Configuration

import scala.util.{Success, Try}
import scala.collection.JavaConverters._
import scala.io.Source

/**
  * Allows to load file contents into configuration:
  *
  * Config example:
  * ```
  *   content = file "/etc/some.field"
  * ```
  *
  * Code to use value:
  *
  * ```
  * import com.impactua.bouncer.commons.utils.ConfigExtensions.Implicits._
  *
  * val config = ConfigFactory.load().filesResolve()
  *
  * val contents = config.getString("content")
  * ```
  *
  */
object ConfigExtensions {
  object Implicits {

    implicit class RichPlayConfig(val c: Configuration) extends AnyVal {
      def filesResolve() = Configuration(
        resolve(c.underlying.root()).get.asInstanceOf[ConfigObject].toConfig
      )
    }

    implicit class RichTypesafeConfig(val c: Config) extends AnyVal {
      def filesResolve() = resolve(c.root()).get.asInstanceOf[ConfigObject].toConfig
    }

    private[this] def resolve(cv: ConfigValue): Try[ConfigValue] = cv.valueType() match {
      case ConfigValueType.OBJECT =>
        val processed = cv.asInstanceOf[ConfigObject].entrySet().asScala
          .flatMap { e => resolve(e.getValue).map(v => e.getKey -> v).toOption }
          .toMap

        Try(ConfigValueFactory.fromMap(processed.asJava))

      case ConfigValueType.STRING if cv.unwrapped().toString.trim.startsWith("file ") =>
        val filename = cv.unwrapped().toString.substring("file ".length)

        asConfigVal {
          val s = Source.fromFile(filename)
          val res = s.mkString
          s.close()
          res
        }


      case ConfigValueType.STRING if cv.unwrapped().toString.trim.startsWith("file! ") =>
        // fails if file doesn't exist
        val filename = cv.unwrapped().toString.substring("file! ".length)
        val s = Source.fromFile(filename)
        val res = s.mkString
        s.close()
        asConfigVal(res)
      case _ =>
        Success(cv)
    }

    private[this] def asConfigVal(any: => AnyRef): Try[ConfigValue] = Try(ConfigValueFactory.fromAnyRef(any))
  }

}
