package com.impactua.bouncer.commons.utils

import java.util.Date

import play.api.data.format.Formatter
import play.api.data.validation.Constraint
import play.api.data.{FormError, Forms, Mapping}

/**
  *
  * @author Yaroslav Derman <yaroslav.derman@gmail.com>
  *         created on 26/07/16
  */
object FormMapping {

  def opt[A](mapping: Mapping[A], constraint: Constraint[Option[A]]): Mapping[Option[A]] = OptMapping(mapping, Seq(constraint))

  def isoDate: Mapping[Date] = {
    def isoDateFormat: Formatter[Date] = new Formatter[Date] {
      def bind(key: String, data: Map[String, String]) = {
        play.api.data.format.Formats.stringFormat.bind(key, data).right.flatMap { s =>
          scala.util.control.Exception.allCatch[Date]
            .either(DateHelpers.readTimestampIso(s))
            .left.map(e => Seq(FormError(key, "error.date", Nil)))
        }
      }

      def unbind(key: String, value: Date) = Map(key -> value.getTime.toString)
    }

    Forms.of(isoDateFormat)
  }

  /**
    * Constructs a simple mapping for a text field (mapped as `scala.Enumeration`)
    * thanks to Leon Radley (https://github.com/leon)
    *
    * For example:
    * {{{
    *   Form("status" -> enum(Status))
    * }}}
    *
    * @param enum the Enumeration#Value
    */
  def enum[E <: Enumeration](enum: E, lowercase: Boolean = false): Mapping[E#Value] = {
    def enumFormat[E <: Enumeration](enum: E, lowercase: Boolean = false): Formatter[E#Value] = new Formatter[E#Value] {
      def bind(key: String, data: Map[String, String]) = {
        play.api.data.format.Formats.stringFormat.bind(key, data).right.flatMap { s =>
          scala.util.control.Exception.allCatch[E#Value]
            .either(enum.values.find(_.toString.equalsIgnoreCase(s)).getOrElse(
              throw new NoSuchElementException(s"No value found for '$s'"))
            )
            .left.map(_ => Seq(FormError(key, "error.enum", Seq(enum.values.iterator.mkString(",")))))
        }
      }

      def unbind(key: String, value: E#Value) = Map(key -> (if(lowercase) value.toString.toLowerCase else value.toString))
    }

    Forms.of(enumFormat(enum, lowercase))
  }

  private case class OptMapping[T](wrapped: Mapping[T], constraints: Seq[Constraint[Option[T]]] = Nil) extends Mapping[Option[T]] {

    override val format: Option[(String, Seq[Any])] = wrapped.format

    val key = wrapped.key

    def verifying(addConstraints: Constraint[Option[T]]*): Mapping[Option[T]] = {
      this.copy(constraints = constraints ++ addConstraints.toSeq)
    }

    def bind(data: Map[String, String]): Either[Seq[FormError], Option[T]] = {
      data.keys.filter(p => p == key || p.startsWith(key + ".") || p.startsWith(key + "["))
        .map(k => data.get(k)).collectFirst { case Some(v) => v }.map { _ =>
        wrapped.bind(data).right.map(Some(_))
      }.getOrElse {
        Right(None)
      }.right.flatMap(applyConstraints)
    }

    def unbind(value: Option[T]): Map[String, String] = {
      value.map(wrapped.unbind).getOrElse(Map.empty)
    }

    def unbindAndValidate(value: Option[T]): (Map[String, String], Seq[FormError]) = {
      val errors = collectErrors(value)
      value.map(wrapped.unbindAndValidate).map(r => r._1 -> (r._2 ++ errors)).getOrElse(Map.empty[String, String] -> errors)
    }

    def withPrefix(prefix: String): Mapping[Option[T]] = {
      copy(wrapped = wrapped.withPrefix(prefix))
    }

    val mappings: Seq[Mapping[_]] = wrapped.mappings

  }

}
