package com.impactua.bouncer.commons.utils

import java.util.UUID

import play.api.libs.json._

import scala.language.implicitConversions

/**
 * @author Yaroslav Derman <yaroslav.derman@gmail.com>.
 * created on 24/09/15.
 */
object EnumJsonFormat {

  def enumReads[E <: Enumeration](enum: E): Reads[E#Value] = new Reads[E#Value] {
    def reads(json: JsValue): JsResult[E#Value] = json match {
      case JsString(s) =>
        enum.values.find(_.toString.equalsIgnoreCase(s)).map(JsSuccess(_)).getOrElse(
          JsError(s"Enumeration expected of type: '${enum.getClass}', but it does not contain the value: '$s'")
        )
      case JsNumber(id) =>
        try {
          JsSuccess(enum(id.toInt))
        } catch {
          case _: Exception =>
            JsError(s"Enumeration expected of type: '${enum.getClass}', but it does not appear to have id: '$id'")
        }
      case _ => JsError("String value expected")
    }
  }

  def enumWrites[E <: Enumeration](text: Boolean = true, lowercase: Boolean = true): Writes[E#Value] = new Writes[E#Value] {
    def writes(v: E#Value): JsValue = {
      text match {
        case true => JsString(if (lowercase) v.toString.toLowerCase else v.toString)
        case false => JsNumber(v.id)
      }
    }
  }

  def enumFormat[E <: Enumeration](enum: E, text: Boolean = true, lowercase: Boolean = true): Format[E#Value] = {
    Format(enumReads(enum), enumWrites(text, lowercase))
  }

}

object UuidJsonFormat {

  implicit def uuidReads: Reads[UUID] = new Reads[UUID] {
    override def reads(json: JsValue): JsResult[UUID] = json match {
      case JsString(s) =>
        try {
          JsSuccess(UUID.fromString(s))
        } catch {
          case e: IllegalArgumentException => JsError(e.getMessage)
        }
      case _ => JsError("String value expected")
    }
  }

  implicit def uuidWrites: Writes[UUID] = new Writes[UUID] {
    override def writes(o: UUID): JsValue = JsString(o.toString)
  }

  implicit def uuidFormat: Format[UUID] = Format(uuidReads, uuidWrites)

}
