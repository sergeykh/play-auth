package com.impactua.bouncer.commons.models

//scalastyle:off magic.number
import java.util.NoSuchElementException

import com.impactua.bouncer.commons.models.exceptions.AppException.CodeConverter
import play.api.http._

import scala.language.implicitConversions

/**
  * Global server error codes
  *
  * @author Sergey Khruschak <sergey.khruschak@gmail.com>
  *         Created on 12/25/15.
  */
object ResponseCode extends Enumeration {

  type Code = ResponseCode.Value

  final val INTERNAL_SERVER_ERROR            = Value(1, "INTERNAL_SERVER_ERROR")
  final val DUPLICATE_REQUEST                = Value(2, "DUPLICATE_REQUEST")
  final val SERVICE_UNAVAILABLE              = Value(3, "SERVICE_UNAVAILABLE")

  // Validation Errors
  final val USER_NOT_FOUND                      = Value(404, "USER_NOT_FOUND")
  final val INVALID_REQUEST                     = Value(408, "INVALID_REQUEST")
  final val APPLICATION_NOT_FOUND               = Value(413, "APPLICATION_NOT_FOUND")
  final val INVALID_TOKEN_CLAIMS                = Value(416, "INVALID_TOKEN_CLAIMS")
  final val EXTERNAL_ACCOUNT_INFO_NOT_FOUND     = Value(417, "EXTERNAL_ACCOUNT_INFO_NOT_FOUND")
  final val INVALID_APPLICATION_SECRET          = Value(426, "INVALID_APPLICATION_SECRET")
  final val INVALID_TOKEN                       = Value(427, "INVALID_TOKEN")
  final val TOKEN_NOT_FOUND                     = Value(428, "TOKEN_NOT_FOUND")
  final val IDENTIFIER_REQUIRED                 = Value(429, "IDENTIFIER_REQUIRED")
  final val BLOCKED_USER                        = Value(431, "BLOCKED_USER")
  final val USER_NOT_CONFIRMED                  = Value(432, "USER_NOT_CONFIRMED")
  final val AUTHORIZATION_FAILED                = Value(433, "AUTHORIZATION_FAILED")
  final val CONFIRM_CODE_NOT_FOUND              = Value(435, "CONFIRM_CODE_NOT_FOUND")
  final val ACCESS_DENIED                       = Value(436, "ACCESS_DENIED")
  final val INVALID_IDENTIFIER                  = Value(437, "INVALID_IDENTIFIER")
  final val INVALID_PASSWORD                    = Value(439, "INVALID_PASSWORD")
  final val EXTERNAL_SERVICE_UNAVAILABLE        = Value(442, "EXTERNAL_SERVICE_UNAVAILABLE")
  final val CONFIRMATION_REQUIRED               = Value(443, "CONFIRMATION_REQUIRED")
  final val ENTITY_NOT_FOUND                    = Value(444, "ENTITY_NOT_FOUND")
  final val ALREADY_EXISTS                      = Value(446, "ALREADY_EXISTS")

  final val EXPIRED_PASSWORD                    = Value(461, "EXPIRED_PASSWORD")
  final val NON_EMPTY_SET                       = Value(462, "NON_EMPTY_SET")
  final val CONCURRENT_MODIFICATION             = Value(463, "CONCURRENT_MODIFICATION")

  implicit val codeToHttpConverter = new CodeConverter[Code] {
    def toHttp(code: Code): Int = code match {
      case IDENTIFIER_REQUIRED | NON_EMPTY_SET => Status.PRECONDITION_FAILED

      case CONFIRMATION_REQUIRED => 428 // Precondition required

      case ALREADY_EXISTS | DUPLICATE_REQUEST | CONCURRENT_MODIFICATION =>
        Status.CONFLICT

      case INVALID_REQUEST | INVALID_TOKEN_CLAIMS | INVALID_APPLICATION_SECRET | INVALID_TOKEN | INVALID_IDENTIFIER | INVALID_PASSWORD =>
        Status.BAD_REQUEST

      case USER_NOT_FOUND | ENTITY_NOT_FOUND | APPLICATION_NOT_FOUND |
           TOKEN_NOT_FOUND | CONFIRM_CODE_NOT_FOUND =>
        Status.NOT_FOUND

      case BLOCKED_USER | USER_NOT_CONFIRMED | ACCESS_DENIED | AUTHORIZATION_FAILED | EXPIRED_PASSWORD =>
        Status.FORBIDDEN

      case SERVICE_UNAVAILABLE =>
        Status.SERVICE_UNAVAILABLE

      case _ =>
        Status.INTERNAL_SERVER_ERROR
    }
  }
}
