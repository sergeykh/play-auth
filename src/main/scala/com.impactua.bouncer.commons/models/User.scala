package com.impactua.bouncer.commons.models

import java.security.{BasicPermission, Permission, Permissions => JPermissions}

import com.impactua.bouncer.commons.models.User.{UserId, UserPermission}
import play.api.libs.functional.syntax._
import play.api.libs.json.Reads._
import play.api.libs.json.{JsPath, Reads, _}

/**
  * Created by Sergey Khruschak on 4/14/16.
  */
case class User(uuid: UserId,
                email: Option[String] = None,
                phone: Option[String] = None,
                firstName: Option[String] = None,
                lastName: Option[String] = None,
                roles: Seq[String] = Seq(),
                flags: Seq[String] = Seq(),
                hierarchy: Seq[String] = Seq(),
                permissions: Seq[String] = Nil) {

  lazy val javaPermissions: JPermissions = {
    val perm = new JPermissions()
    permissions.foreach(p => perm.add(UserPermission(p)))
    perm
  }

  val upperPermissions = permissions.map(_.toUpperCase)

  def checkIdentifier(anyId: String) = {
    anyId != null && (email.contains(anyId.toLowerCase) || phone.contains(anyId) || uuid.toString == anyId)
  }

  def hasPermission(p: Permission): Boolean = javaPermissions.implies(p)

  def hasAllPermission(p: Permission*): Boolean = p.forall(hasPermission)

  def hasAnyPermission(p: Permission*): Boolean = p.exists(hasPermission)

  def hasPermission(s: String) = upperPermissions.contains(s.toUpperCase)

  def hasFlag(s: String) = flags.contains(s)

  def branch: Option[String] = hierarchy.headOption
}

object User {
  type UserId = Long

  case class UserPermission(name: String) extends BasicPermission(name.toUpperCase)

  val fullReader: Reads[User] = (
    (JsPath \ "uuid").read[Long] and
      (JsPath \ "email").readNullable[String] and
      (JsPath \ "phone").readNullable[String] and
      (JsPath \ "firstName").readNullable[String] and
      (JsPath \ "lastName").readNullable[String] and
      (JsPath \ "roles").read[Seq[String]].orElse(Reads.pure(Nil)) and
      (JsPath \ "flags").read[Seq[String]].orElse(Reads.pure(Nil)) and
      (JsPath \ "hierarchy").read[Seq[String]].orElse(Reads.pure(Nil)) and
      (JsPath \ "permissions").read[Seq[String]].orElse(Reads.pure(Nil))
    )(User.apply _)

  val shortReader: Reads[User] = (
      (JsPath \ "id").read[Long] and
      (JsPath \ "em").readNullable[String] and
      (JsPath \ "ph").readNullable[String] and
      (JsPath \ "fn").readNullable[String] and
      (JsPath \ "ln").readNullable[String] and
      (JsPath \ "rol").read[Seq[String]].orElse(Reads.pure(Nil)) and
      (JsPath \ "flg").read[Seq[String]].orElse(Reads.pure(Nil)) and
      (JsPath \ "hrc").read[Seq[String]].orElse(Reads.pure(Nil)) and
      (JsPath \ "prm").read[Seq[String]].orElse(Reads.pure(Nil))
    )(User.apply _)

  implicit val reader: Reads[User] = fullReader orElse shortReader

  implicit val writer = new Writes[User] {
    def writes(u: User): JsValue = JsObject(
      Json.obj(
        "uuid" -> u.uuid,
        "email" -> u.email,
        "phone" -> u.phone,
        "firstName" -> u.firstName,
        "lastName" -> u.lastName,
        "roles" -> u.roles,
        "flags" -> u.flags,
        "hierarchy" -> u.hierarchy,
        "permissions" -> u.permissions
      ).fields.filter {
        case (_, JsNull) => false
        case (_, JsArray(value)) if value.isEmpty => false
        case _ => true
      }
    )
  }
}

