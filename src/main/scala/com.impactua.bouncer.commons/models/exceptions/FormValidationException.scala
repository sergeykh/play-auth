package com.impactua.bouncer.commons.models.exceptions

import play.api.data.Form

/**
  * Created by sergeykhruschak on 7/22/16.
  */
case class FormValidationException[T](form: Form[T]) extends Exception(form.errors.mkString)
