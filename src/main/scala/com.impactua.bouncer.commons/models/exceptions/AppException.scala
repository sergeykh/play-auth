package com.impactua.bouncer.commons.models.exceptions


case class AppException[T](code: T, message: String, httpCode: Int) extends Exception {

  override def getMessage: String = message + ", code: " + code

  override def toString: String = "Application exception: " + getMessage
}

object AppException {
  trait CodeConverter[-T] {
    def toHttp(code: T): Int
  }

  def apply[T](code: T, message: String)(implicit codeConverter: CodeConverter[T]): AppException[T] =
    AppException(code, message, codeConverter.toHttp(code))
}