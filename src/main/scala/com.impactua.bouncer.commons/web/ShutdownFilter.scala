package com.impactua.bouncer.commons.web

import java.util.concurrent.atomic.AtomicBoolean
import javax.inject.{Inject, Singleton}

import akka.stream.Materializer
import com.impactua.bouncer.commons.models.ResponseCode
import com.impactua.bouncer.commons.utils.Logging
import play.api.Configuration
import play.api.inject.DefaultApplicationLifecycle
import play.api.libs.json.Json
import play.api.mvc.Results._
import play.api.mvc.{Filter, RequestHeader, Result}
import sun.misc.{Signal, SignalHandler}

import scala.compat.Platform
import scala.concurrent.duration._
import scala.concurrent.Future
import scala.language.postfixOps

/**
  *
  * @author Yaroslav Derman <yaroslav.derman@gmail.com>
  *         created on 21/06/17
  */
@Singleton
class ShutdownFilter @Inject()(appShutdownRegister: ApplicationShutdownRegister)(implicit val mat: Materializer) extends Filter with Logging {

  override def apply(nextFilter: (RequestHeader) => Future[Result])(request: RequestHeader): Future[Result] = {
    if (appShutdownRegister.isShutdown) {
      val unavailable = ResponseCode.SERVICE_UNAVAILABLE

      Future.successful(
        ServiceUnavailable(Json.obj(
          "code" -> unavailable.id,
          "error" -> unavailable.toString,
          "message" -> "service unavailable",
          "timestamp" -> Platform.currentTime
        ))
      )
    } else {
      nextFilter(request)
    }
  }

}

@Singleton
class ApplicationShutdownRegister @Inject()(lifecycle: DefaultApplicationLifecycle, conf: Configuration) extends Logging {

  private val delayTime = conf.getOptional[Duration]("filters.shutdown.delay-time").getOrElse(5.seconds).toMillis

  private val isShuttingDown = new AtomicBoolean(false)

  SignalHandlerInstaller.installHandler("TERM", "INT") { (oldHandler, signal) =>
    log.warn(s"Shutdown signal $signal received, going into shutdown state with delay $delayTime ms")
    isShuttingDown.set(true)
    Thread.sleep(delayTime)
    log.warn(s"Terminating application")
    oldHandler.handle(signal)
  }

  def isShutdown: Boolean = isShuttingDown.get()
}

object SignalHandlerInstaller {

  def installHandler(signals: String*)(handlerFn: (SignalHandler, Signal) => Unit): Unit = {
    signals.foreach { signal =>
      val handler = new InternalHandler(handlerFn)
      handler.oldHandler = Some(Signal.handle(new Signal(signal), handler))
    }
  }

  private[this] class InternalHandler(handlerFn: (SignalHandler, Signal) => Unit) extends SignalHandler {
    var oldHandler: Option[SignalHandler] = None

    override def handle(signal: Signal): Unit = {
      handlerFn(oldHandler.get, signal)
    }
  }

}